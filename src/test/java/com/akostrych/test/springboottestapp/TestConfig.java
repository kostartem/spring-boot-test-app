package com.akostrych.test.springboottestapp;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.akostrych.test.springboottestapp")
public class TestConfig {
}
