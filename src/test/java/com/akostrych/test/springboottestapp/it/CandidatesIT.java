package com.akostrych.test.springboottestapp.it;

import com.akostrych.test.springboottestapp.TestConfig;
import com.akostrych.test.springboottestapp.TestSpringBootTestAppApplication;
import com.akostrych.test.springboottestapp.controllers.request.CreateCandidateRequest;
import com.akostrych.test.springboottestapp.data.CandidateDetailsDto;
import com.akostrych.test.springboottestapp.data.CandidatesSpokenLanguageDto;
import com.akostrych.test.springboottestapp.data.ExperienceDto;
import com.akostrych.test.springboottestapp.data.SkillDto;
import com.akostrych.test.springboottestapp.repository.CandidateRepository;
import com.akostrych.test.springboottestapp.repository.models.Gender;
import com.akostrych.test.springboottestapp.repository.models.SkillLevel;
import com.akostrych.test.springboottestapp.repository.models.SpeakingLevel;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {TestSpringBootTestAppApplication.class, TestConfig.class})
@WebAppConfiguration
@Transactional
public class CandidatesIT {

    private static final String V_1_CANDIDATES = "/v1/candidates";
    private static final String JAVA = "Java";
    private static final String PYTHON = "Python";
    private static final String PHONE_NUMBER = "1234567890";
    private static final String TITLE = "Software Engineer";
    private static final String DETAILS = "Some details about the candidate";
    private static final String MIDDLE_NAME = "Middle";
    private static final String LAST_NAME = "Doe";
    private static final String AVATAR_123 = "avatar123";
    private static final String FIRST_NAME = "John";
    private static final String EMAIL = "example@email.com";
    private static final String EXPERIENCE_FIRST_TITLE = "Software Developer";
    private static final String EXPERIENCE_SECOND_TITLE = "Intern";
    private static final BigDecimal SALARY_EXPECTATIONS = new BigDecimal("50000.00");
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private CandidateRepository candidateRepository;
    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
    }

    @AfterEach
    public void tearDown() {
        candidateRepository.deleteAll();
    }

    @Test
    @DisplayName("Test candidates creation")
    public void testCandidatesCreation() throws Exception {
        var createCandidateRequest = getSampleCreateCandidateRequest();

        this.mockMvc.perform(post(V_1_CANDIDATES)
                        .content(objectMapper.writeValueAsString(createCandidateRequest))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        var createdCandidate = candidateRepository.findAll().get(0);

        assertEquals(FIRST_NAME, createdCandidate.getFirstName());
        assertEquals(LAST_NAME, createdCandidate.getLastName());
        assertEquals(MIDDLE_NAME, createdCandidate.getMiddleName());
        assertEquals(EMAIL, createdCandidate.getEmail());
        assertEquals(AVATAR_123, createdCandidate.getAvatarId());
        assertEquals(SALARY_EXPECTATIONS, createdCandidate.getSalaryExpectations());
        assertEquals(DETAILS, createdCandidate.getDetails());
        assertEquals(TITLE, createdCandidate.getTitle());
        assertEquals(PHONE_NUMBER, createdCandidate.getPhoneNumber());
        assertEquals(Gender.MALE, createdCandidate.getGender());
        assertNotNull(createdCandidate.getCreatedDate());

        assertNotNull(createdCandidate.getSkills());
        assertEquals(2, createdCandidate.getSkills().size());
        assertEquals(JAVA, createdCandidate.getSkills().get(0).getName());
        assertEquals(SkillLevel.EXPERT, createdCandidate.getSkills().get(0).getLevel());
        assertEquals(PYTHON, createdCandidate.getSkills().get(1).getName());
        assertEquals(SkillLevel.ADVANCED, createdCandidate.getSkills().get(1).getLevel());

        assertNotNull(createdCandidate.getExperiences());
        assertEquals(2, createdCandidate.getExperiences().size());

        assertNotNull(createdCandidate.getSpokenLanguages());
        assertEquals(2, createdCandidate.getSpokenLanguages().size());
    }

    @Test
    @DisplayName("Test candidates creation process with invalid parameters")
    public void testCandidatesCreation_withInvalidParameters() throws Exception {

        //8 invalid parameters
        var createCandidateRequest = CreateCandidateRequest.builder().build();

        var result = this.mockMvc.perform(post(V_1_CANDIDATES)
                        .content(objectMapper.writeValueAsString(createCandidateRequest))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        var responseContent = result.getResolvedException().getMessage();
        assertTrue(responseContent.contains("with 8 errors"));

        // email empty
        createCandidateRequest = getSampleCreateCandidateRequest();
        createCandidateRequest.setEmail(null);

        result = this.mockMvc.perform(post(V_1_CANDIDATES)
                        .content(objectMapper.writeValueAsString(createCandidateRequest))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        responseContent = result.getResolvedException().getMessage();
        assertTrue(responseContent.contains("Field error in object 'createCandidateRequest' " +
                "on field 'email': rejected value [null]"));

        // firstName empty
        createCandidateRequest = getSampleCreateCandidateRequest();
        createCandidateRequest.setFirstName("");

        result = this.mockMvc.perform(post(V_1_CANDIDATES)
                        .content(objectMapper.writeValueAsString(createCandidateRequest))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        responseContent = result.getResolvedException().getMessage();
        assertTrue(responseContent.contains("Field error in object 'createCandidateRequest' on " +
                "field 'firstName': rejected value []"));


        // skills empty
        createCandidateRequest = getSampleCreateCandidateRequest();
        createCandidateRequest.setSkills(new ArrayList<>());

        result = this.mockMvc.perform(post(V_1_CANDIDATES)
                        .content(objectMapper.writeValueAsString(createCandidateRequest))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

        responseContent = result.getResolvedException().getMessage();
        assertTrue(responseContent.contains("Field error in object 'createCandidateRequest' on field 'skills': " +
                "rejected value [[]]"));
    }

    @Test
    @DisplayName("Test candidates deletion")
    public void testCandidatesDeletion() throws Exception {
        var createCandidateRequest = getSampleCreateCandidateRequest();

        this.mockMvc.perform(post(V_1_CANDIDATES)
                        .content(objectMapper.writeValueAsString(createCandidateRequest))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        var candidateId = candidateRepository.findAll().get(0).getId();

        this.mockMvc.perform(delete(V_1_CANDIDATES + "/{candidateId}", candidateId))
                .andExpect(MockMvcResultMatchers.status().isOk());

        var allCandidates = candidateRepository.findAll();
        assertEquals(0, allCandidates.size());
    }

    @Test
    @DisplayName("Test get candidate details")
    public void testCandidatesFiltering() throws Exception {
        var createCandidateRequest = getSampleCreateCandidateRequest();

        this.mockMvc.perform(post(V_1_CANDIDATES)
                        .content(objectMapper.writeValueAsString(createCandidateRequest))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        var candidateId = candidateRepository.findAll().get(0).getId();
        var result = this.mockMvc.perform(get(V_1_CANDIDATES + "/{candidateId}", candidateId))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        var candidateResponseString = result.getResponse().getContentAsString();
        var candidateResponse = objectMapper.readValue(candidateResponseString,
                CandidateDetailsDto.class);

        assertEquals(FIRST_NAME, candidateResponse.getFirstName());
        assertEquals(LAST_NAME, candidateResponse.getLastName());
        assertEquals(MIDDLE_NAME, candidateResponse.getMiddleName());
        assertEquals(SALARY_EXPECTATIONS, candidateResponse.getSalaryExpectations());
        assertEquals(DETAILS, candidateResponse.getDetails());
        assertEquals(TITLE, candidateResponse.getTitle());
        assertEquals(PHONE_NUMBER, candidateResponse.getPhoneNumber());
        assertEquals(Gender.MALE, candidateResponse.getGender());
        assertEquals(2, candidateResponse.getSkills().size());
        assertEquals(2, candidateResponse.getSpokenLanguages().size());
        assertEquals(2, candidateResponse.getExperiences().size());
    }

    public CreateCandidateRequest getSampleCreateCandidateRequest() {
        return CreateCandidateRequest.builder()
                .email(EMAIL)
                .firstName(FIRST_NAME)
                .avatarId(AVATAR_123)
                .lastName(LAST_NAME)
                .middleName(MIDDLE_NAME)
                .salaryExpectations(SALARY_EXPECTATIONS)
                .details(DETAILS)
                .title(TITLE)
                .phoneNumber(PHONE_NUMBER)
                .gender(Gender.MALE)
                .skills(List.of(
                        SkillDto.builder().name(JAVA).level(SkillLevel.EXPERT).build(),
                        SkillDto.builder().name(PYTHON).level(SkillLevel.ADVANCED).build()
                ))
                .spokenLanguages(List.of(
                        CandidatesSpokenLanguageDto.builder().languageId(1).level(SpeakingLevel.FLUENT).build(),
                        CandidatesSpokenLanguageDto.builder().languageId(2).level(SpeakingLevel.CONVERSATIONAL).build()
                ))
                .experiences(List.of(
                        ExperienceDto.builder()
                                .title(EXPERIENCE_FIRST_TITLE)
                                .fromDate(LocalDateTime.of(2018, 1, 1, 0, 0))
                                .toDate(LocalDateTime.of(2022, 12, 31, 23, 59))
                                .build(),
                        ExperienceDto.builder()
                                .title(EXPERIENCE_SECOND_TITLE)
                                .fromDate(LocalDateTime.of(2016, 5, 1, 0, 0))
                                .toDate(LocalDateTime.of(2016, 8, 31, 23, 59))
                                .build()
                ))
                .build();
    }
}
