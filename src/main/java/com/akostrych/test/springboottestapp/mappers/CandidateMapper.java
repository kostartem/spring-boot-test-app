package com.akostrych.test.springboottestapp.mappers;

import com.akostrych.test.springboottestapp.controllers.request.CreateCandidateRequest;
import com.akostrych.test.springboottestapp.data.CandidateDetailsDto;
import com.akostrych.test.springboottestapp.data.CandidateSummaryDto;
import com.akostrych.test.springboottestapp.repository.models.CandidateEntity;
import org.mapstruct.*;

import java.time.LocalDateTime;
import java.util.List;

import static java.time.ZoneOffset.UTC;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;

@Mapper(componentModel = "spring",
        uses = {SpokenLanguagesMapper.class},
        builder = @Builder(disableBuilder = true))
public interface CandidateMapper {
    CandidateDetailsDto asDto(CandidateEntity source);

    CandidateSummaryDto asSummary(CandidateEntity source);

    List<CandidateSummaryDto> asSummaries(List<CandidateEntity> source);

    @Mapping(target = "createdDate", expression = "java(getCurrentDate())")
    CandidateEntity asEntity(CreateCandidateRequest source);

    @AfterMapping
    default void mapAfterForCreation(@MappingTarget CandidateEntity target) {
        emptyIfNull(target.getExperiences())
                .forEach(experienceEntity -> experienceEntity.setCandidate(target));
        emptyIfNull(target.getSpokenLanguages())
                .forEach(spokenLanguage -> spokenLanguage.setCandidate(target));
        emptyIfNull(target.getSkills())
                .forEach(skillEntity -> skillEntity.setCandidate(target));
    }

    default LocalDateTime getCurrentDate() {
        return LocalDateTime.now(UTC);
    }
}
