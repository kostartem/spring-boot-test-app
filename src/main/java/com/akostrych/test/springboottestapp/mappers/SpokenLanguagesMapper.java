package com.akostrych.test.springboottestapp.mappers;


import com.akostrych.test.springboottestapp.data.CandidatesSpokenLanguageDto;
import com.akostrych.test.springboottestapp.repository.models.CandidatesSpokenLanguageEntity;
import com.akostrych.test.springboottestapp.repository.models.SpokenLanguageEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SpokenLanguagesMapper {

    @Mapping(target = "spokenLanguage", expression = "java(getSpokenLanguageEntity(source))")
    CandidatesSpokenLanguageEntity map(CandidatesSpokenLanguageDto source);

    @Mapping(target = "languageId", source = "spokenLanguage.id")
    CandidatesSpokenLanguageDto map(CandidatesSpokenLanguageEntity source);

    default SpokenLanguageEntity getSpokenLanguageEntity(CandidatesSpokenLanguageDto source){
        return SpokenLanguageEntity.builder()
                .id(source.getLanguageId())
                .build();
    }

    List<CandidatesSpokenLanguageEntity> map(List<CandidatesSpokenLanguageDto> sources);
}
