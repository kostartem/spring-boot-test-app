package com.akostrych.test.springboottestapp.controllers.response;

import com.akostrych.test.springboottestapp.data.CandidateSummaryDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CandidateFilterResponse {
    private List<CandidateSummaryDto> data;
    private Integer page;
    private Integer size;
    private Long totalPages;
}
