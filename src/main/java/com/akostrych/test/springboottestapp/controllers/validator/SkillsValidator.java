package com.akostrych.test.springboottestapp.controllers.validator;

import com.akostrych.test.springboottestapp.data.SkillDto;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.apache.commons.collections4.ListUtils.emptyIfNull;

public class SkillsValidator implements ConstraintValidator<SkillsValid, List<SkillDto>> {

    @Override
    public boolean isValid(List<SkillDto> skillDtos, ConstraintValidatorContext constraintValidatorContext) {
        return !(getUniqueSkillNames(skillDtos).size() < emptyIfNull(skillDtos).size());
    }

    private Set<String> getUniqueSkillNames(List<SkillDto> skillDtos) {
        return emptyIfNull(skillDtos).stream()
                .map(SkillDto::getName)
                .collect(Collectors.toSet());
    }
}
