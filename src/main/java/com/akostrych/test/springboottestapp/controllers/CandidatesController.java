package com.akostrych.test.springboottestapp.controllers;

import com.akostrych.test.springboottestapp.controllers.request.CreateCandidateRequest;
import com.akostrych.test.springboottestapp.controllers.response.CandidateFilterResponse;
import com.akostrych.test.springboottestapp.data.CandidateDetailsDto;
import com.akostrych.test.springboottestapp.services.CandidateService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/v1/candidates")
@RequiredArgsConstructor
public class CandidatesController {

    private final CandidateService candidateService;

    @GetMapping
    private CandidateFilterResponse filter(@RequestParam(name = "searchStr", required = false) String searchStr,
                                           @RequestParam(name = "skills", required = false) List<String> skills,
                                           @RequestParam(name = "spokenLanguage", required = false) List<Integer> spokenLanguage,
                                           @RequestParam(name = "salaryFrom", required = false) BigDecimal salaryFrom,
                                           @RequestParam(name = "salaryTo", required = false) BigDecimal salaryTo,
                                           @RequestParam(name = "page", defaultValue = "0") Integer page,
                                           @RequestParam(name = "size", defaultValue = "10") @Max(1000) Integer size) {
        return candidateService.filterCandidates(searchStr, skills, spokenLanguage,
                salaryFrom, salaryTo, page, size);
    }

    @PostMapping
    private void createCandidate(@RequestBody @Valid CreateCandidateRequest createCandidateRequest) {
        candidateService.createCandidate(createCandidateRequest);
    }

    @GetMapping("/{candidateId}")
    private CandidateDetailsDto getCandidateDetails(@PathVariable(name = "candidateId") UUID candidateId) {
        return candidateService.getCandidateDetails(candidateId);
    }

    @DeleteMapping("/{candidateId}")
    private void deleteCandidate(@PathVariable(name = "candidateId") UUID candidateId) {
        candidateService.deleteCandidate(candidateId);
    }

}
