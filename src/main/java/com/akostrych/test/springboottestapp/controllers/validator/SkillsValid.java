package com.akostrych.test.springboottestapp.controllers.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = SkillsValidator.class)
@Documented
public @interface SkillsValid {
    String message() default "Skill names should be unique per request";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
