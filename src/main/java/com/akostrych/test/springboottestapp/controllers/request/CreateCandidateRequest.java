package com.akostrych.test.springboottestapp.controllers.request;

import com.akostrych.test.springboottestapp.controllers.validator.SkillsValid;
import com.akostrych.test.springboottestapp.data.CandidatesSpokenLanguageDto;
import com.akostrych.test.springboottestapp.data.ExperienceDto;
import com.akostrych.test.springboottestapp.data.SkillDto;
import com.akostrych.test.springboottestapp.repository.models.Gender;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateCandidateRequest {

    @NotBlank
    private String email;
    @NotBlank
    private String firstName;
    private String avatarId;
    @NotBlank
    private String lastName;
    private String middleName;
    @NotNull
    private BigDecimal salaryExpectations;
    private String details;
    @NotBlank
    private String title;
    private String phoneNumber;
    @NotNull
    private Gender gender;

    @NotEmpty
    @SkillsValid
    private List<SkillDto> skills;
    @NotEmpty
    private List<CandidatesSpokenLanguageDto> spokenLanguages;
    private List<ExperienceDto> experiences;
}
