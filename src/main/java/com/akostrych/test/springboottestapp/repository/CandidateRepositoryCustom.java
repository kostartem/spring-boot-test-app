package com.akostrych.test.springboottestapp.repository;

import com.akostrych.test.springboottestapp.repository.models.CandidateEntity;

import java.math.BigDecimal;
import java.util.List;

public interface CandidateRepositoryCustom {

    Long getTotalElements(String searchStr, List<String> skills, List<Integer> spokenLanguages,
                          BigDecimal salaryFrom, BigDecimal salaryTo);

    List<CandidateEntity> filterCandidates(String searchStr, List<String> skills, List<Integer> spokenLanguages,
                                           BigDecimal salaryFrom, BigDecimal salaryTo, Integer page, Integer size);
}
