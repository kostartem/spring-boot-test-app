package com.akostrych.test.springboottestapp.repository.models;

public enum SpeakingLevel {
    BASIC,
    CONVERSATIONAL,
    FLUENT,
    NATIVE
}
