package com.akostrych.test.springboottestapp.repository;

import com.akostrych.test.springboottestapp.repository.models.CandidateEntity;
import com.akostrych.test.springboottestapp.repository.models.CandidatesSpokenLanguageEntity;
import com.akostrych.test.springboottestapp.repository.models.SkillEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.*;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class CandidateRepositoryCustomImpl implements CandidateRepositoryCustom {

    private final EntityManager entityManager;

    @Override
    public Long getTotalElements(String searchStr, List<String> skills, List<Integer> spokenLanguages,
                                 BigDecimal salaryFrom, BigDecimal salaryTo) {
        var cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);

        Root<CandidateEntity> root = addConditions(searchStr, skills, spokenLanguages, salaryFrom, salaryTo, cb, cq);

        cq.select(cb.count(root));

        return entityManager.createQuery(cq)
                .getSingleResult();
    }

    @Override
    public List<CandidateEntity> filterCandidates(String searchStr, List<String> skills, List<Integer> spokenLanguages,
                                                  BigDecimal salaryFrom, BigDecimal salaryTo,
                                                  Integer page, Integer size) {

        var cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<CandidateEntity> cq = cb.createQuery(CandidateEntity.class);

        Root<CandidateEntity> root = addConditions(searchStr, skills, spokenLanguages, salaryFrom, salaryTo, cb, cq);

        var query = entityManager.createQuery(cq);

        cq.orderBy(cb.asc(root.get("createdDate")));

        return query.setFirstResult(size * page)
                .setMaxResults(size)
                .getResultList();
    }

    private Root<CandidateEntity> addConditions(String searchStr, List<String> skills, List<Integer> spokenLanguages,
                                                BigDecimal salaryFrom, BigDecimal salaryTo, CriteriaBuilder cb,
                                                CriteriaQuery<?> cq) {
        Root<CandidateEntity> root = cq.from(CandidateEntity.class);

        var andPredicates = new ArrayList<Predicate>();

        var searchStrPredicates = getSearchStrPredicates(searchStr, cb, root);

        if (!searchStrPredicates.isEmpty()) {
            andPredicates.add(cb.or(searchStrPredicates.toArray(new Predicate[]{})));
        }

        if (skills != null && !skills.isEmpty()) {
            Join<CandidateEntity, SkillEntity> candidateSkills =
                    root.join("skills");
            andPredicates.add(candidateSkills.get("name").in(skills));
        }

        if (spokenLanguages != null && !spokenLanguages.isEmpty()) {
            Join<CandidateEntity, CandidatesSpokenLanguageEntity> candidateLanguages =
                    root.join("spokenLanguages").join("spokenLanguage");
            andPredicates.add(candidateLanguages.get("id").in(spokenLanguages));
        }

        if (salaryFrom != null) {
            andPredicates.add(cb.greaterThanOrEqualTo(root.get("salaryExpectations"),
                    salaryFrom));
        }

        if (salaryTo != null) {
            andPredicates.add(cb.lessThanOrEqualTo(root.get("salaryExpectations"),
                    salaryTo));
        }

        cq.where(andPredicates.toArray(new Predicate[]{}));
        return root;
    }

    private ArrayList<Predicate> getSearchStrPredicates(String searchStr, CriteriaBuilder cb, Root<CandidateEntity> root) {
        var orPredicates = new ArrayList<Predicate>();

        if (StringUtils.isNotBlank(searchStr)) {
            var likeString = "%" + searchStr + "%";

            Join<CandidateEntity, SkillEntity> candidateSkills =
                    root.join("skills");

            orPredicates.add(cb.like(candidateSkills.get("name"), likeString));
            orPredicates.add(cb.like(root.get("firstName"), likeString));
            orPredicates.add(cb.like(root.get("lastName"), likeString));
            orPredicates.add(cb.like(root.get("details"), likeString));
            orPredicates.add(cb.like(root.get("title"), likeString));
        }
        return orPredicates;
    }


}
