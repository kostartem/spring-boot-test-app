package com.akostrych.test.springboottestapp.repository;

import com.akostrych.test.springboottestapp.repository.models.CandidateEntity;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface CandidateRepository extends JpaRepository<CandidateEntity, UUID>, CandidateRepositoryCustom {

    @EntityGraph(value = "CandidateEntity.details")
    Optional<CandidateEntity> findById(UUID id);

}
