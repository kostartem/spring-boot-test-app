package com.akostrych.test.springboottestapp.repository.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import java.util.UUID;

@Entity
@Data
@Table(name = "T_CANDIDATES_SPOKEN_LANGUAGES_MAPPING")
public class CandidatesSpokenLanguageEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    @Column(name = "LEVEL", length = 20)
    private SpeakingLevel level;

    @ManyToOne
    @JoinColumn(name = "CANDIDATE_ID", referencedColumnName = "ID")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private CandidateEntity candidate;

    @ManyToOne
    @JoinColumn(name = "SPOKEN_LANGUAGE_ID", referencedColumnName = "ID")
    private SpokenLanguageEntity spokenLanguage;
}
