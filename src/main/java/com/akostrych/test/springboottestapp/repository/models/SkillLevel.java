package com.akostrych.test.springboottestapp.repository.models;

public enum SkillLevel {
    BEGINNER,
    INTERMEDIATE,
    ADVANCED,
    EXPERT
}
