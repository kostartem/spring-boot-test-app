package com.akostrych.test.springboottestapp.repository.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "T_CANDIDATES")
@NamedEntityGraph(name = "CandidateEntity.details",
        attributeNodes = {
                @NamedAttributeNode("skills"),
                @NamedAttributeNode("spokenLanguages"),
                @NamedAttributeNode("experiences")
        })
@NamedEntityGraph(name = "CandidateEntity.summary",
        attributeNodes = {
                @NamedAttributeNode("experiences")
        })
public class CandidateEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    @Column(name = "EMAIL", nullable = false, length = 100)
    private String email;

    @Column(name = "FIRST_NAME", nullable = false, length = 100)
    private String firstName;

    @Column(name = "AVATAR_ID", length = 1000)
    private String avatarId;

    @Column(name = "LAST_NAME", nullable = false, length = 100)
    private String lastName;

    @Column(name = "MIDDLE_NAME", length = 100)
    private String middleName;

    @Column(name = "SALARY_EXPECTATIONS", nullable = false)
    private BigDecimal salaryExpectations;

    @Column(name = "DETAILS")
    private String details;

    @Column(name = "TITLE", nullable = false, length = 1000)
    private String title;

    @Column(name = "PHONE_NUMBER", length = 25)
    private String phoneNumber;

    @Column(name = "GENDER", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "CREATED_DATE")
    private LocalDateTime createdDate;

    @Column(name = "MODIFIED_DATE")
    private LocalDateTime modifiedDate;

    @OneToMany(mappedBy = "candidate", cascade = CascadeType.ALL)
    private List<SkillEntity> skills;

    @OneToMany(mappedBy = "candidate", cascade = CascadeType.ALL)
    private List<CandidatesSpokenLanguageEntity> spokenLanguages;

    @OneToMany(mappedBy = "candidate", cascade = CascadeType.ALL)
    private List<ExperienceEntity> experiences;
}
