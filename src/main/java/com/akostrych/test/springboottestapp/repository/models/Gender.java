package com.akostrych.test.springboottestapp.repository.models;

public enum Gender {
    MALE, FEMALE
}
