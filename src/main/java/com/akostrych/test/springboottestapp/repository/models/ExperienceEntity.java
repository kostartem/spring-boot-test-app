package com.akostrych.test.springboottestapp.repository.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@Table(name = "T_EXPERIENCES")
public class ExperienceEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    @Column(name = "TITLE", nullable = false, length = 100)
    private String title;

    @Column(name = "FROM_DATE", nullable = false)
    private LocalDateTime fromDate;

    @Column(name = "TO_DATE", nullable = false)
    private LocalDateTime toDate;

    @ManyToOne
    @JoinColumn(name = "CANDIDATE_ID", referencedColumnName = "ID")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private CandidateEntity candidate;
}