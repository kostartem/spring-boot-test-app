package com.akostrych.test.springboottestapp.repository.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "T_SPOKEN_LANGUAGES")
public class SpokenLanguageEntity {
    @Id
    private int id;

    @Column(name = "NAME", nullable = false, length = 100)
    private String name;
}
