package com.akostrych.test.springboottestapp.services;

import com.akostrych.test.springboottestapp.controllers.request.CreateCandidateRequest;
import com.akostrych.test.springboottestapp.controllers.response.CandidateFilterResponse;
import com.akostrych.test.springboottestapp.data.CandidateDetailsDto;
import com.akostrych.test.springboottestapp.mappers.CandidateMapper;
import com.akostrych.test.springboottestapp.repository.CandidateRepository;
import com.akostrych.test.springboottestapp.repository.models.CandidateEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class CandidateService {
    private final CandidateRepository candidateRepository;
    private final CandidateMapper candidateMapper;

    public CandidateDetailsDto getCandidateDetails(UUID candidateId) {
        log.debug("Retrieving candidate [{}] details. Started", candidateId);

        var candidateModel = getCandidateEntity(candidateId);

        log.debug("Retrieving candidate [{}] details. Finished", candidateId);
        return candidateMapper.asDto(candidateModel);
    }

    private CandidateEntity getCandidateEntity(UUID candidateId) {
        return candidateRepository.findById(candidateId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Candidate with id [%s] was not found", candidateId)));
    }

    public CandidateFilterResponse filterCandidates(String searchStr, List<String> skills,
                                                    List<Integer> spokenLanguages, BigDecimal salaryFrom,
                                                    BigDecimal salaryTo, Integer page, Integer size) {
        log.debug("Filtering candidates. Started");

        return CandidateFilterResponse.builder()
                .page(page)
                .size(size)
                .totalPages(candidateRepository.getTotalElements(searchStr, skills, spokenLanguages, salaryFrom, salaryTo))
                .data(candidateMapper.asSummaries(
                        candidateRepository.filterCandidates(searchStr, skills, spokenLanguages, salaryFrom,
                                salaryTo, page, size)))
                .build();
    }

    @Transactional
    public void createCandidate(CreateCandidateRequest createCandidateRequest) {
        log.debug("Creating candidate [{}]. Started", createCandidateRequest.getEmail());

        var newCandidate = candidateMapper.asEntity(createCandidateRequest);
        candidateRepository.save(newCandidate);

        log.debug("Creating candidate [{}]. Finished", createCandidateRequest.getEmail());
    }

    @Transactional
    public void deleteCandidate(UUID candidateId) {
        log.debug("Deleting candidate [{}]. Started", candidateId);

        candidateRepository.deleteById(candidateId);

        log.debug("Deleting candidate [{}]. Finished", candidateId);
    }
}
