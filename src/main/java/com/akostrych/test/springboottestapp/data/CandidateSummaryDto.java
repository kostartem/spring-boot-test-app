package com.akostrych.test.springboottestapp.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CandidateSummaryDto {
    private UUID id;
    private String firstName;
    private String avatarId;
    private String lastName;
    private String details;
    private String title;
    private BigDecimal salaryExpectations;

    private List<SkillDto> skills;
}
