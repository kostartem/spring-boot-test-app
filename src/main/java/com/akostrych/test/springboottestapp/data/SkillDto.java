package com.akostrych.test.springboottestapp.data;

import com.akostrych.test.springboottestapp.repository.models.SkillLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SkillDto {
    private String name;
    private SkillLevel level;
}
