package com.akostrych.test.springboottestapp.data;

import com.akostrych.test.springboottestapp.repository.models.Gender;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CandidateDetailsDto {
    private String firstName;
    private String avatarId;
    private String lastName;
    private String middleName;
    private BigDecimal salaryExpectations;
    private String details;
    private String title;
    private String phoneNumber;
    private Gender gender;

    private List<SkillDto> skills;
    private List<CandidatesSpokenLanguageDto> spokenLanguages;
    private List<ExperienceDto> experiences;
}
