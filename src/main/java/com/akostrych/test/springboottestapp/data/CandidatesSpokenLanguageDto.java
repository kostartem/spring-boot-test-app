package com.akostrych.test.springboottestapp.data;

import com.akostrych.test.springboottestapp.repository.models.SpeakingLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CandidatesSpokenLanguageDto {
    private Integer languageId;
    private SpeakingLevel level;
}
