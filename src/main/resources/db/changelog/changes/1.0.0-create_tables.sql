--liquibase formatted sql

--changeset akostych:001

CREATE TABLE T_CANDIDATES
(
    id                  uuid PRIMARY KEY       DEFAULT gen_random_uuid(),
    email               VARCHAR(100)  NOT NULL,
    first_name          VARCHAR(100)  NOT NULL,
    avatar_id           VARCHAR(1000),
    last_name           VARCHAR(100)  NOT NULL,
    middle_name         VARCHAR(100),
    salary_expectations DECIMAL       NOT NULL DEFAULT 0.0,
    details             VARCHAR,
    title               VARCHAR(1000) NOT NULL,
    phone_number        VARCHAR(25),
    gender              VARCHAR(50)   NOT NULL,

    created_date        TIMESTAMP              DEFAULT CURRENT_TIMESTAMP,
    modified_date       TIMESTAMP
);

CREATE TABLE T_SPOKEN_LANGUAGES
(
    id   int PRIMARY KEY,
    name varchar(100) NOT NULL
);

CREATE TABLE T_CANDIDATES_SPOKEN_LANGUAGES_MAPPING
(
    id                 uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    level              varchar(20),
    candidate_id       uuid,
    spoken_language_id int,
    CONSTRAINT T_CANDIDATES_MAPPING_FK
        FOREIGN KEY (candidate_id)
            REFERENCES T_CANDIDATES (id),
    CONSTRAINT T_SPOKEN_LANGUAGES_MAPPING_FK
        FOREIGN KEY (spoken_language_id)
            REFERENCES T_SPOKEN_LANGUAGES (id)
);

INSERT INTO T_SPOKEN_LANGUAGES (id, name)
VALUES (1, 'English'),
       (2, 'Ukrainian'),
       (3, 'French'),
       (4, 'Spanish'),
       (5, 'German');


CREATE TABLE T_EXPERIENCES
(
    id           uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    title        VARCHAR(100) NOT NULL,
    from_date    timestamp    NOT NULL,
    to_date      timestamp    NOT NULL,
    candidate_id uuid,
    CONSTRAINT T_EXPERIENCE_MAPPING_T_CANDIDATES_FK
        FOREIGN KEY (candidate_id)
            REFERENCES T_CANDIDATES (id)
);

CREATE TABLE T_SKILLS
(
    id           uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    name         VARCHAR(100) NOT NULL,
    level        VARCHAR(100) NOT NULL,
    candidate_id uuid,
    CONSTRAINT T_EXPERIENCE_MAPPING_T_CANDIDATES_FK
        FOREIGN KEY (candidate_id)
            REFERENCES T_CANDIDATES (id)
);

--rollback DROP TABLE "T_SKILLS";
--rollback DROP TABLE "T_EXPERIENCES";
--rollback DROP TABLE "T_SPOKEN_LANGUAGES";
--rollback DROP TABLE "T_CANDIDATES_SPOKEN_LANGUAGES_MAPPING";
--rollback DROP TABLE "T_USERS";
