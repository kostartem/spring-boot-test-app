FROM eclipse-temurin:17-jre

LABEL tag=spring-boot-test-app

RUN adduser --system --group teaching_me
ENV PROJECT_HOME /home/spring-boot-test-app

COPY --chown=spring-boot-test-app target/*.jar $PROJECT_HOME/spring-boot-test-app.jar
WORKDIR $PROJECT_HOME

ENV SPRING_PROFILES_ACTIVE=test

CMD ["java","-jar","./spring-boot-test-app.jar"]
