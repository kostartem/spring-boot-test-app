# spring-boot-test-app

Demo spring boot application which contains REST api for candidates CRUD operations.

#### Technologies and libraries used in this app

1. Spring(Spring boot, spring data, core) 

2. Liquibase - used to load database schema and control its updates

3. Mapstruct - used to simplify entities to dtos mappers creation

4. Testcontainers, Junit5 - used to create component test for this application

5. Dockerfile and docker-compose - used to simplify applications infrastructure setup


## Getting started

To build docker image for spring-boot-test-app service you need to run the next command:
    
    mvn clean install && docker build -t server-app-demo:latest ./

Build docker image for frontend app with command from root folder in repository(https://gitlab.com/kostartem/react-fe-test-app):

    docker build -t react-fe-demo:latest ./

Up docker compose to start full application infrastructure run from current folder:
    
    docker compose up
